测试 mysql binlog 获取测试（使用 https://github.com/siddontang/go-mysql）：

	需要mysql（5.6 5.7）的配置项：（my.ini 或 my.conf）
    server-id=1  （任何值皆可以）
    gtid_mode=ON
    log_slave_updates=1
    enforce_gtid_consistency=1
    log-bin="your binlog path and name"
    binlog_format=ROW
    
    
运行 : 进入 src 目录，修改为你对应的数据库情况，即可 go run test.go 看到binlog的变化的记录。