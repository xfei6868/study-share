﻿package main

import (
    "github.com/siddontang/go-mysql/replication"
    "github.com/siddontang/go-mysql/mysql"
    "os"
)

func main() {  
	// Create a binlog syncer with a unique server id, the server id must be different from other MySQL's. 
	// flavor is mysql or mariadb
	syncer := replication.NewBinlogSyncer(100, "mysql")

	// Register slave, the MySQL master is at 127.0.0.1:3306, with user root and an empty password
	syncer.RegisterSlave("127.0.0.1", 9808, "root", "xufei123")

	// Start sync with sepcified binlog file and position
	streamer, _ := syncer.StartSync(mysql.Position{"binlog.000002", 0})

	// or you can start a gtid replication like
	// streamer, _ := syncer.StartSyncGTID(gtidSet)
	// the mysql GTID set likes this "de278ad0-2106-11e4-9f8e-6edd0ca20947:1-2"
	// the mariadb GTID set likes this "0-1-100"

	for {
		ev, _ := streamer.GetEvent()
		// Dump event
		ev.Dump(os.Stdout)
	}
}